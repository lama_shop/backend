import { DataSourceOptions } from 'typeorm/data-source/DataSourceOptions'

export const databaseConfig: DataSourceOptions = {
  type: 'mysql',
  host: 'localhost',
  port: 8889,
  username: 'root',
  password: 'root',
  database: 'lama_shop',
  entities: [
    __dirname + '/../**/*.entity{.ts,.js}'
  ],
  synchronize: true,
  logging: true
}


