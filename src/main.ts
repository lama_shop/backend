import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { ValidationPipe } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { EnvironmentVariables } from './interfaces/environmentVariables'

async function bootstrap () {
  const app = await NestFactory.create(AppModule)
  app.enableCors()
  app.useGlobalPipes(new ValidationPipe({
    // disableErrorMessages: true,
    transformOptions: {
      enableImplicitConversion: true
    },
    whitelist: true,
    forbidNonWhitelisted: true,
    transform: true
  }))

  const configService: ConfigService<EnvironmentVariables> = app.get(ConfigService)
  const port = configService.get('port', 8080)

  console.log(`App listen on port ${port}`)
  await app.listen(port)
}

bootstrap()
