import { Inject, Injectable } from '@nestjs/common'
import { CreateProductDto } from './dto/create-product.dto'
import { UpdateProductDto } from './dto/update-product.dto'
import { Repository } from 'typeorm'
import { Product } from './entities/product.entity'
import { ProductI } from '../shared_contracts/product/product'
import { MulterDiskUploadedFiles } from '../interfaces/files'
import * as fs from 'fs'
import * as path from 'path'
import { storageDir } from '../utilts/storage'
import { PHOTO_PATH, PRODUCT_REPOSITORY } from './product.contracts'
import { ConfigService } from '@nestjs/config'
import { EnvironmentVariables } from '../interfaces/environmentVariables'

@Injectable()
export class ProductService {
  constructor (
    @Inject(PRODUCT_REPOSITORY) private productRepository: Repository<Product>,
    private configService: ConfigService<EnvironmentVariables>
  ) {
  }

  public mapProduct (product: Product): ProductI {
    return {
      title: product.title,
      description: product.description,
      regularPrice: {
        gross: product.regularGrossPrice,
        net: product.regularNetPrice
      },
      finalPrice: {
        gross: product.finalGrossPrice,
        net: product.finalNetPrice
      },
      discountOff: {
        gross: product.discountOffGross,
        net: product.discountOffNet
      },
      image: {
        alt: product.imageAlt,
        url: `http://${this.configService.get('baseUrl')}:${this.configService.get('port')}/${PHOTO_PATH}/${product.photoFileName}`
      },
      sku: product.sku
    }
  }

  public async create (createProductDto: CreateProductDto, files: MulterDiskUploadedFiles): Promise<ProductI> {
    const photo = files?.photo?.[0] ?? null

    const productEntity = await this.productRepository.findOne({ where: { sku: createProductDto.sku } })
    if (productEntity) {
      throw 'Product with given sku exist in database'
    }

    const product = this.productRepository.create(createProductDto)

    try {
      if (photo) {
        product.photoFileName = photo.filename
      }

      await this.productRepository.save(product)
    } catch (e) {
      try {
        if (photo) {
          fs.unlinkSync(path.join(storageDir(), 'product-photos', photo.filename))
          // fs.unlinkSync(photo.filename)
        }
      } catch (e2) {
        throw e2
      }
      throw e
    }
    return this.mapProduct(product)
  }

  public async findAll (): Promise<ProductI[]> {
    const products = await this.productRepository.find()
    return products.map(product => this.mapProduct(product))
  }

  findOne (id: number) {
    return `This action returns a #${id} product`
  }

  update (id: number, updateProductDto: UpdateProductDto) {
    return `This action updates a #${id} product`
  }

  remove (id: number) {
    return `This action removes a #${id} product`
  }

  public async getPhoto (id: string) {
    try {
      const one = await this.productRepository.findOne({ where: { photoFileName: id } })
      if (!one) {
        throw new Error('No object found!')
      }

      if (!one.photoFileName) {
        throw new Error('No photo in this entity!')
      }

      return one
    } catch (e) {
      throw e
      // res.json({ error: e.message })
    }
  }
}
