import { Module } from '@nestjs/common'
import { ProductService } from './product.service'
import { ProductController } from './product.controller'
import { productProviders } from './product.providers'
import { DatabaseModule } from '../database/database.module'
import { ConfigModule } from '@nestjs/config'

@Module({
  imports: [DatabaseModule, ConfigModule],
  controllers: [ProductController],
  exports: [ProductService],
  providers: [...productProviders, ProductService]
})
export class ProductModule {}
