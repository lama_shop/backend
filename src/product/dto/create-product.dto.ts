import { IsNotEmpty, IsNumber, IsString } from 'class-validator'

export class CreateProductDto {
  @IsString()
  @IsNotEmpty()
  sku: string

  @IsString()
  @IsNotEmpty()
  title: string

  @IsString()
  @IsNotEmpty()
  description: string

  @IsNumber()
  @IsNotEmpty()
  finalGrossPrice: number

  @IsNumber()
  @IsNotEmpty()
  finalNetPrice: number

  @IsNumber()
  @IsNotEmpty()
  regularGrossPrice: number

  @IsNumber()
  @IsNotEmpty()
  regularNetPrice: number

  @IsNumber()
  @IsNotEmpty()
  discountOffGross: number

  @IsNumber()
  @IsNotEmpty()
  discountOffNet: number

  @IsString()
  imageAlt: string
}
