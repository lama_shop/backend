import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFiles, Res
} from '@nestjs/common'
import { ProductService } from './product.service'
import { CreateProductDto } from './dto/create-product.dto'
import { UpdateProductDto } from './dto/update-product.dto'
import { ProductI } from '../shared_contracts/product/product'
import { FileFieldsInterceptor } from '@nestjs/platform-express'
import * as path from 'path'
import { multerStorage, storageDir } from '../utilts/storage'
import { MulterDiskUploadedFiles } from '../interfaces/files'
import { ConfigService } from '@nestjs/config'

@Controller('product')
export class ProductController {
  constructor (private readonly productService: ProductService, private configService: ConfigService) {
  }

  @Post()
  @UseInterceptors(FileFieldsInterceptor([
    {
      name: 'photo', maxCount: 1
    }
  ], { storage: multerStorage(path.join(storageDir(), 'product-photos')) }))
  public async create (
    @Body() createProductDto: CreateProductDto,
    @UploadedFiles() files: MulterDiskUploadedFiles
  ) {
    return await this.productService.create(createProductDto, files)
  }

  @Get()
  public async findAll (): Promise<ProductI[]> {
    return await this.productService.findAll()
  }

  @Get(':id')
  findOne (@Param('id') id: string) {
    return this.productService.findOne(+id)
  }

  @Patch(':id')
  update (@Param('id') id: string, @Body() updateProductDto: UpdateProductDto) {
    return this.productService.update(+id, updateProductDto)
  }

  @Delete(':id')
  remove (@Param('id') id: string) {
    return this.productService.remove(+id)
  }

  @Get('/photo/:id')
  public async getPhoto (@Param('id') id: string, @Res() res: any): Promise<any> {
    const image = await this.productService.getPhoto(id)
    res.sendFile(image.photoFileName, { root: path.join(storageDir(), 'product-photos') })
  }
}
