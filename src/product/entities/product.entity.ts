import { Column, Entity, PrimaryColumn } from 'typeorm'

@Entity()
export class Product {
  @PrimaryColumn({ unique: true })
  sku: string

  @Column()
  title: string

  @Column({
    type: 'longtext'
  })
  description: string

  @Column({
    type: 'float',
    precision: 10,
    scale: 2
  })
  finalGrossPrice: number

  @Column({
    type: 'float',
    precision: 10,
    scale: 2
  })
  finalNetPrice: number

  @Column({
    type: 'float',
    precision: 10,
    scale: 2
  })
  regularGrossPrice: number

  @Column({
    type: 'float',
    precision: 10,
    scale: 2
  })
  regularNetPrice: number

  @Column({
    type: 'float',
    precision: 10,
    scale: 2
  })
  discountOffGross: number

  @Column({
    type: 'float',
    precision: 10,
    scale: 2
  })
  discountOffNet: number

  @Column()
  imageAlt: string

  @Column({
    default: null,
    nullable: true
  })
  photoFileName: string

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  createdAt: Date
}
