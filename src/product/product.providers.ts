import { DataSource } from 'typeorm'
import { Product } from './entities/product.entity'
import { DATA_SOURCE } from '../database/database.contracts'
import { PRODUCT_REPOSITORY } from './product.contracts'

export const productProviders = [
  {
    provide: PRODUCT_REPOSITORY,
    useFactory: (dataSource: DataSource) => dataSource.getRepository(Product),
    inject: [DATA_SOURCE]
  }
]
