export interface EnvironmentVariables {
  port: number,
  baseUrl: string
}
