import { DataSource } from 'typeorm'
import { Cart } from './entities/cart.entity'
import { CART_ITEM_REPOSITORY, CART_REPOSITORY } from './cart.contracts'
import { DATA_SOURCE } from '../database/database.contracts'
import { CartItem } from './entities/cart-item.entity'
import { PRODUCT_REPOSITORY } from '../product/product.contracts'
import { Product } from '../product/entities/product.entity'

export const cartProviders = [
  {
    provide: CART_REPOSITORY,
    useFactory: (dataSource: DataSource) => dataSource.getRepository(Cart),
    inject: [DATA_SOURCE]
  },
  {
    provide: CART_ITEM_REPOSITORY,
    useFactory: (dataSource: DataSource) => dataSource.getRepository(CartItem),
    inject: [DATA_SOURCE]
  },
  {
    provide: PRODUCT_REPOSITORY,
    useFactory: (dataSource: DataSource) => dataSource.getRepository(Product),
    inject: [DATA_SOURCE]
  }
]
