import { Inject, Injectable } from '@nestjs/common'
import { CreateCartDto } from './dto/create-cart.dto'
import { UpdateCartDto } from './dto/update-cart.dto'
import { Repository } from 'typeorm'
import { Cart } from './entities/cart.entity'
import { CART_ITEM_REPOSITORY, CART_REPOSITORY } from './cart.contracts'
import { AddCartItemDto, RemoveCartItemDto, UpdateCartItemDto } from './dto/cart-item.dto'
import { CartItem } from './entities/cart-item.entity'
import { PHOTO_PATH, PRODUCT_REPOSITORY } from '../product/product.contracts'
import { Product } from '../product/entities/product.entity'
import { CartI, CartItemI, CreateEmptyCartI } from '../shared_contracts/cart/cart'
import { ProductService } from '../product/product.service'
import { ProductI } from '../shared_contracts/product/product'
import { ConfigService } from '@nestjs/config'
import { EnvironmentVariables } from '../interfaces/environmentVariables'

@Injectable()
export class CartService {
  constructor (
    @Inject(CART_REPOSITORY) private cartRepository: Repository<Cart>,
    @Inject(CART_ITEM_REPOSITORY) private cartItemRepository: Repository<CartItem>,
    @Inject(PRODUCT_REPOSITORY) private productRepository: Repository<Product>,
    @Inject(ProductService) private productService: ProductService,
    private configService: ConfigService<EnvironmentVariables>
  ) {
  }

  public mapProduct (product: Product): ProductI {
    console.log('mapProduct')
    console.log(product.title)
    return {
      title: product.title,
      description: product.description,
      regularPrice: {
        gross: product.regularGrossPrice,
        net: product.regularNetPrice
      },
      finalPrice: {
        gross: product.finalGrossPrice,
        net: product.finalNetPrice
      },
      discountOff: {
        gross: product.discountOffGross,
        net: product.discountOffNet
      },
      image: {
        alt: product.imageAlt,
        url: `http://${this.configService.get('baseUrl')}:${this.configService.get('port')}/${PHOTO_PATH}/${product.photoFileName}`
      },
      sku: product.sku
    }
  }

  public mapCartItem (cartItem: CartItem): CartItemI {
    return {
      id: cartItem.id,
      quantity: cartItem.quantity,
      createdAt: cartItem.createdAt,
      product: this.mapProduct(cartItem.product)
    }
  }

  public mapCart (cart: Cart): CartI {
    return {
      id: cart.id,
      items: cart.cartItem.map((cartItem) => this.mapCartItem(cartItem)),
      createdAt: cart.createdAt
    }
  }

  create (createCartDto: CreateCartDto) {
    return 'This action adds a new cart'
  }

  findAll () {
    return `This action returns all cart`
  }

  findOne (id: number) {
    return `This action returns a #${id} cart`
  }

  update (id: number, updateCartDto: UpdateCartDto) {
    return `This action updates a #${id} cart`
  }

  remove (id: number) {
    return `This action removes a #${id} cart`
  }

  public async isValidCart (cartId: string): Promise<boolean> {
    try {
      return !!(await this.cartRepository.findOneBy({ id: cartId }))
    } catch (e) {
      return e
    }
  }

  public async createEmptyCart (): Promise<CreateEmptyCartI> {
    const cart = await this.cartRepository.save({})
    return {
      cartId: cart.id
    }
  }

  public async addCartItem (cartId: string, addCartItemDto: AddCartItemDto): Promise<CartItemI> {
    console.log('%c addCartItem', 'color:yellow')
    console.log(cartId)
    console.log(addCartItemDto)
    try {
      const cart = await this.cartRepository.findOneOrFail({
        where: {
          id: cartId
        }, relations: {
          cartItem: {
            product: true
          }
        }
      })
      const chosenCartItemInCart = cart.cartItem.find(cartItem => cartItem.product.sku === addCartItemDto.productSku)

      // Check if chosen product is in cart
      if (!!chosenCartItemInCart) {

        // Change quantity of product which is already in cart
        return await this.updateCartItem(
          cartId,
          { productId: chosenCartItemInCart.id, quantity: addCartItemDto.quantity + chosenCartItemInCart.quantity }
        )
      }

      const productEntity = await this.productRepository.findOneOrFail({ where: { sku: addCartItemDto.productSku } })

      const newCartItem = this.cartItemRepository.create({ ...addCartItemDto, product: { ...productEntity } })
      const cartItemEntity = await this.cartItemRepository.save(newCartItem)
      cart.cartItem.push(cartItemEntity)

      await this.cartRepository.save(cart)

      return this.mapCartItem(newCartItem)
    } catch (e) {
      throw e
    }
  }

  public async updateCartItem (cartId: string, updateCartItemDto: UpdateCartItemDto): Promise<CartItemI> {
    try {
      const cartItemEntity = await this.cartItemRepository.findOneOrFail({
        where: {
          id: updateCartItemDto.productId, cartItem: {
            id: cartId
          }
        },
        relations: {
          product: true
        }
      })
      cartItemEntity.quantity = updateCartItemDto.quantity

      console.log('v11')

      await this.cartItemRepository.save(cartItemEntity)

      return this.mapCartItem(cartItemEntity)
    } catch (e) {
      throw e
    }
  }

  public async getCart (cartId: string): Promise<CartI> {
    const cartEntity = await this.cartRepository.findOneOrFail({
      where: { id: cartId }, relations: {
        cartItem: {
          product: true
        }
      }
    })

    // const cartItem = await this.cartItemRepository
    //   .createQueryBuilder('cart_item')
    //   .where('cart_item.cartItem = :id', { id: cartId })
    //   .leftJoin('cart_item.cartItem', 'cart')
    //   .leftJoinAndSelect('cart_item.product', 'product')
    //   .select(['SUM(cart_item.quantity) as abc'])
    //   .groupBy("cart_item.productSku")
    //   .getRawMany()
    // console.log('cartItem')
    // console.log(cartItem)
    // console.log(cartItem.length)

    return this.mapCart(cartEntity)
  }

  public async removeCartItem (cartId: string, removeCartItemDto: RemoveCartItemDto): Promise<CartItemI> {
    try {
      const cart = await this.getCart(cartId)
      const cartItem = cart.items.find(item => item.product.sku === removeCartItemDto.productSku)

      if (!cartItem) {
        return
      }
      const cartItemEntity = await this.cartItemRepository.findOne({
        where: { id: cartItem.id }, relations: {
          product: true
        }
      })

      return this.mapCartItem(await this.cartItemRepository.remove(cartItemEntity))
    } catch (e) {
      throw new Error(e)
    }
  }
}
