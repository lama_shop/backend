import { IsInt, IsString } from 'class-validator'
import { AddCartItem, RemoveCartItem, UpdateCartItem } from '../../shared_contracts/cart/cart'

export class AddCartItemDto implements AddCartItem {
  @IsString()
  productSku: string

  @IsInt()
  quantity: number
}


export class UpdateCartItemDto implements UpdateCartItem {
  @IsString()
  productId: string

  @IsInt()
  quantity: number
}

export class RemoveCartItemDto implements RemoveCartItem {
  @IsString()
  productSku: string
}
