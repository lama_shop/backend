import { Controller, Get, Post, Body, Patch, Param, Delete, Put } from '@nestjs/common'
import { CartService } from './cart.service'
import { AddCartItemDto, RemoveCartItemDto, UpdateCartItemDto } from './dto/cart-item.dto'
import { CartItem } from './entities/cart-item.entity'
import { CartI, CartItemI, CreateEmptyCartI } from '../shared_contracts/cart/cart'

@Controller('cart')
export class CartController {
  constructor (private readonly cartService: CartService) {
  }

  @Put()
  public async createEmptyCart (): Promise<CreateEmptyCartI> {
    return await this.cartService.createEmptyCart()
  }

  @Put('/:cartId')
  public async addCartItem (
    @Body() AddCartItemDto: AddCartItemDto,
    @Param('cartId') cartId: string
  ): Promise<CartItemI> {
    return await this.cartService.addCartItem(cartId, AddCartItemDto)
  }

  @Put('/item/:cartId')
  public async updateCartItem (
    @Body() updateCartItemDto: UpdateCartItemDto,
    @Param('cartId') cartId: string
  ): Promise<CartItemI> {
    return await this.cartService.updateCartItem(cartId, updateCartItemDto)
  }

  @Get('/:cartId')
  public async getCart (@Param('cartId') cartId: string): Promise<CartI> {
    if (!(await this.cartService.isValidCart(cartId))) {
      return null
    }
    return await this.cartService.getCart(cartId)
  }

  // @Get()
  // findAll() {
  //   return this.cartService.findAll();
  // }
  //
  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.cartService.findOne(+id);
  // }
  //
  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateCartDto: UpdateCartDto) {
  //   return this.cartService.update(+id, updateCartDto);
  // }
  //
  @Delete(':cartId')
  public async remove (
    @Param('cartId') cartId: string,
    @Body() removeCartItemDto: RemoveCartItemDto
  ): Promise<CartItemI> {
    return (await this.cartService.removeCartItem(cartId, removeCartItemDto))
  }
}
