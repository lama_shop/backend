import { Module } from '@nestjs/common'
import { CartService } from './cart.service'
import { CartController } from './cart.controller'
import { cartProviders } from './cart.providers'
import { DatabaseModule } from '../database/database.module'
import { ProductModule } from '../product/product.module'
import { ConfigModule } from '@nestjs/config'

@Module({
  imports: [
    DatabaseModule,
    ProductModule,
    ConfigModule
  ],
  controllers: [CartController],
  providers: [...cartProviders, CartService]
})
export class CartModule {}
