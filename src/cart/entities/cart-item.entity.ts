import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { Cart } from './cart.entity'
import { Product } from '../../product/entities/product.entity'

@Entity()
export class CartItem {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @ManyToOne(type => Product, entity => entity.sku)
  @JoinColumn()
  product: Product

  @Column({
    type: 'int'
  })
  quantity: number

  @ManyToOne(type => Cart, (entity) => entity.id)
  @JoinColumn()
  cartItem: Cart

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  createdAt: Date
}

