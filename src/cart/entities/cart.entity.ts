import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { CartItem } from './cart-item.entity'

@Entity()
export class Cart {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @OneToMany(type => CartItem, (entity) => entity.cartItem)
  @JoinColumn()
  cartItem: CartItem[]

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  createdAt: Date
}
